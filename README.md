# Hello, World!

This [NodeJS](https://nodejs.org) application starts an HTTP
server on port `3000`, responding with the text `Hello, World!`.

```
$ npm start

> hello-world@1.0.0 start /path/to/node-hello-world
> node index.js

Server running at http://0.0.0.0:3000/
```

```
$ curl -i http://localhost:3000
HTTP/1.1 200 OK
Content-Type: text/plain
Date: Fri, 15 May 2020 15:53:43 GMT
Connection: keep-alive
Content-Length: 14

Hello, World!
```

## Challenge

-   Login to the `bti360-interview` [AWS Console](https://bti360-interview.signin.aws.amazon.com/console)
-   Launch an EC2 instance in the provided AWS account
    -   Use the Northern Virginia Region, `us-east-1`
    -   Amazon Linux 2
    -   t2.micro (Free tier)
-   Remotely login to the instance
-   download this code from [`s3://bti360-interviews/node-hello-world.tgz`](https://s3.console.aws.amazon.com/s3/object/bti360-interviews/node-hello-world.tgz?region=us-east-1)
-   Create a Docker image that serves the Hello, World! application
    -   You will need to install Docker. Amazon Linux is a RedHat based system.
-   Run the Docker container and verify the `Hello, World!` response is accessible on `http://${EC2_PUBLIC_IP}`
    -   The `docker run` port mapping argument is `-p ${HOST_PORT}:${CONTAINER_PORT}`

## Starting the Server

```bash
npm start
```

## Useful Links

-   [Dockerfile Reference](https://docs.docker.com/engine/reference/builder/)
-   [`docker run` Reference](https://docs.docker.com/engine/reference/run/)
